<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("location: index.php");
}

//Base Template
$templateParams["pagina"]="registrati";
$templateParams["body"] = "template/datiBancariForm.php";

$templateParams["js"] = array("js/jquery-3.5.1.min.js","js/controlsDatiBancari.js");

require 'template/baseSmallHeader.php';
?>