<?php
    require_once("../bootstrap.php");
    $msg = "";

    // If the user clicked the add to cart button on the product page we can check for the form data
    if (isUserLoggedIn() && isset($_POST["codModello"], $_POST["misura"], $_GET["store"]) && is_numeric($_POST["misura"])) {
        // Set the post variables so we easily identify them, also make sure they are integer
        $codModello = $_POST["codModello"];
        $misura = $_POST["misura"];

        if (!empty($codModello)) {
            // Product exists in database, now we can create/update the session variable for the cart
            if (isset($_SESSION["cart"]) && is_array($_SESSION["cart"])) {
                if (array_key_exists($codModello, $_SESSION["cart"]) && array_key_exists($misura, $_SESSION["cart"][$codModello])) {
                    // Product and size exists in cart so just update the quantity
                    $quantitàdisponibile = $dbh->getDisponibilitàProdotto($codModello, $misura);
                    $quantitàdesiderata = $_SESSION["cart"][$codModello][$misura];
                    if($quantitàdesiderata < $quantitàdisponibile[0]["quantità"]) {
                        $_SESSION["cart"][$codModello][$misura] += 1;
                        $_SESSION["productsInCart"] += 1;
                        $msg = "Prodotto aggiunto al carrello!";
                    } else {
                        $msg = "Ops! Mi sa che sono esaurite le scorte :(";
                    }
                } else {
                    // Product is not in cart so add it or it's in it but in a different size
                    $_SESSION["cart"][$codModello][$misura] = 1;
                    $_SESSION["productsInCart"] += 1;
                    $msg = "Prodotto aggiunto al carrello!";
                } 
            } else {
                // There are no products in cart, this will add the first product to cart
                $_SESSION["cart"][$codModello] = array($misura => 1);
                $_SESSION["productsInCart"] += 1;
                $msg = "Prodotto aggiunto al carrello!";
            }
        } else {
            $msg = "Ops! Qualcosa è andato storto :(";
        }
    } else {
        $msg = "Ricordati di selezionare la misura!";
    }

    header('Content-Type: application/json');
    echo json_encode(array("msg" => $msg, "productsInCart" => $_SESSION["productsInCart"]));
?>