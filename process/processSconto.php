<?php
require_once("../bootstrap.php");

    if(isUserLoggedIn() && isset($_POST["importo"], $_GET["btn"])){
        $importo = $_POST["importo"];
        $_SESSION["importo"] = $importo;
        if(isset($_POST["codSconto"]) && $_POST["codSconto"]!="" && !isset($_SESSION["giàscontato"])) {
            $sconto = $_POST["codSconto"];
            $result = $dbh->checkCodiceSconto($_SESSION["user"], $sconto);
            if(count($result)==0) {
                echo "Ci dispiace ma non puoi usufruire di questo sconto";
            } else {
                $perc = $dbh->getSconto($sconto);
                $importoScontato = $importo - (($importo / 100.00) * $perc[0]["percentuale"]);
                $_SESSION["importo"] = $importoScontato;
                $_SESSION["sconto"] = $perc[0]["percentuale"];
                $_SESSION["giàscontato"] = "si";
                echo $importoScontato;
            }
        } else if($_GET["btn"] == "riscuoti") {
            echo "0";
        } else {
            unset($_SESSION["giàscontato"]);
            echo "datiBancari.php";
        }
    }
?>