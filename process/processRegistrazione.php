<?php
require_once '../bootstrap.php';
if(isset($_POST["name"], $_POST["surname"], $_POST["email"],  $_POST["password"])){
		$name = $_POST["name"];
		$surname = $_POST["surname"];
		$email = $_POST["email"];
		$password = $_POST["password"];
		
		$login_result = $dbh->checkEmail($_POST["email"]);
		if(count($login_result)!=0){
			echo "Errore! Account già registrato";
		}
		else{
			$result = $dbh->insertUtente($name, $surname, $email, $password);
			if($result){
				registerLoggedUser($email);
				$_SESSION["productsInCart"] = 0;
				$_SESSION["cart"] = array();
				$notifica = getNotifica("benvenuto", NULL, NULL);
				$res = $dbh->insertNotifica($email, $notifica["oggetto"], $notifica["testo"], NULL, "BENVENUTO10");
				mail($email, $notifica["oggetto"], $notifica["testo"], 'From:laRinascenteShoes.Spa@gmail.com');
				echo "logged";
			} else {
				echo "Ops! Qualcosa è andato storto";
			}
		}
	}
?>