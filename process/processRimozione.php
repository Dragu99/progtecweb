<?php
require_once("../bootstrap.php");

    if(isUserLoggedIn() && isset($_POST["codModello"], $_POST["misura"])){
        $codModello = $_POST["codModello"];
        $misura = $_POST["misura"];
        $_SESSION["cart"][$codModello][$misura] -= 1;
        $_SESSION["productsInCart"] -= 1;
        if ($_SESSION["cart"][$codModello][$misura] == 0) {
            unset($_SESSION["cart"][$codModello][$misura]);
            if (empty($_SESSION["cart"][$codModello])) {
                unset($_SESSION["cart"][$codModello]);
            }
        }
        echo $codModello;
    }
?>