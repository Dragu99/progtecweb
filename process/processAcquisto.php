<?php
require_once("../bootstrap.php");

    if(isUserLoggedIn() && isset($_POST["cardNumber"], $_POST["cardCode"], $_POST["expirationDate"], $_POST["address"], $_POST["city"], $_POST["cap"])){
        if(isset($_SESSION["importo"])) {
          $importo = $_SESSION["importo"];
          $email = $_SESSION["user"];
            if(isset($_SESSION["sconto"])){
                $sconto = $_SESSION["sconto"];
                $codOrdine = $dbh->insertAcquisto($email, (float)$importo, $sconto);
            } else {
                $codOrdine = $dbh->insertAcquisto($email, (float)$importo);
            }
            if($codOrdine == 0){
                echo "Errore! Dati non corretti";
            }
            else{
                foreach($_SESSION["cart"] as $codModello => $misure) {
                    foreach($misure as $misura => $quantità) {
                        $destinatario = $dbh->getShoppingManager($codModello);
                        $prodotto["codModello"] = $codModello;
                        $prodotto["misura"] = $misura;
                        $notifica = getNotifica("acquisto prodotto", NULL, $prodotto);
                        $dbh->insertNotifica($destinatario[0]["e_mail"], $notifica["oggetto"], $notifica["testo"]);
                        mail($destinatario[0]["e_mail"], $notifica["oggetto"], $notifica["testo"], 'From:laRinascenteShoes.Spa@gmail.com');
                        $quantità = $dbh->getDisponibilitàProdotto($codModello, $misura);
                        if($quantità[0]["quantità"] == 0) {
                            $notifica = getNotifica("esaurimento scorte", NULL, $prodotto);
                            $dbh->insertNotifica($destinatario[0]["e_mail"], $notifica["oggetto"], $notifica["testo"]);
                            mail($destinatario[0]["e_mail"], $notifica["oggetto"], $notifica["testo"], 'From:laRinascenteShoes.Spa@gmail.com');
                        }
                    }
                }
                $notifica = getNotifica("ordine effettuato", $codOrdine);
                $dbh->insertNotifica($_SESSION["user"], $notifica["oggetto"], $notifica["testo"], $codOrdine);
				mail($_SESSION["user"], $notifica["oggetto"], $notifica["testo"], 'From:laRinascenteShoes.Spa@gmail.com');
                $_SESSION["cart"] = array();
                $_SESSION["productsInCart"] = 0;
                unset($_SESSION["sconto"]);
                unset($_SESSION["importo"]);
                echo "success";
            }
        } else {
          echo "Ops! Qualcosa è andato storto :(";
        }
    }
?>