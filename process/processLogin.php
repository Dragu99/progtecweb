<?php
require_once("../bootstrap.php");

if(isset($_POST["email"], $_POST["password"])){
    $email = $_POST["email"];
    $password = $_POST["password"];
    $result = $dbh->checkLogin($email, $password);
    if(count($result)==0){
		echo "Errore! Username o password non corretti";
    }
    else{
        registerLoggedUser($result[0]["e_mail"]);
        if($result[0]["venditore"]) {
            $_SESSION["venditore"] = $result[0]["marchio"];
            echo $result[0]["marchio"];
        } else {
            $last = $dbh->getUltimoAcquisto($result[0]["e_mail"]);
            $date = dateDiffFromNow($last[0]["data"]);
            $res = $dbh->checkCodiceSconto($result[0]["e_mail"], "REGALO20");
            if(count($last) > 0 && $date > 90 && count($res) == 0) {
                $notifica = getNotifica("codice sconto", NULL, NULL);
                $dbh->insertNotifica($result[0]["e_mail"], $notifica["oggetto"], $notifica["testo"], NULL, "REGALO20");
				mail($result[0]["e_mail"], $notifica["oggetto"], $notifica["testo"], 'From:laRinascenteShoes.Spa@gmail.com');
            }
            $_SESSION["productsInCart"] = 0;
            $_SESSION["cart"] = array();
            echo "utente";
        }
    }
}
?>