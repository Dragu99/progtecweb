<?php
    require_once("../bootstrap.php");
    $msg = "";

    // If the user clicked the add to cart button on the product page we can check for the form data
    if (isUserLoggedIn() && isset($_POST["codModello"], $_POST["misura"], $_POST["quantità"], $_GET["store"]) && is_numeric($_POST["misura"])) {
        // Set the post variables so we easily identify them, also make sure they are integer
        $codModello = $_POST["codModello"];
        $misura = $_POST["misura"];
        $quantità = $_POST["quantità"];

        if(!empty($codModello) && $quantità > 0) {
            $result = $dbh->updateDisponibilitàProdotto($codModello, $misura, $quantità);
            if($result) {
                //STAMPA MESSAGGIO DI SUCCESSO
                $msg = "Prodotti aggiunti con successo!";
            } else {
                //STAMPA MESSAGGIO DI FALLIMENTO
                $msg = "Inserimento prodotti fallito :(";
            }
        }
    } else {
        $msg = "Ricordati di inserire tutti i dati!";
    }
    echo $msg;
?>