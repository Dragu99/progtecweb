<?php
    require_once("../bootstrap.php");
    $msg = "";

    // If the user clicked the add to cart button on the product page we can check for the form data
    if (isUserLoggedIn() && isset($_POST["nome"], $_POST["misura"], $_POST["quantità"], $_POST["prezzo"], $_POST["descrizione"], $_FILES["foto"], $_GET["store"])) {
        // Set the post variables so we easily identify them, also make sure they are integer
        $nome = $_POST["nome"];
        $misura = $_POST["misura"];
        $quantità = $_POST["quantità"];
        $prezzo = $_POST["prezzo"];
        $descrizione = $_POST["descrizione"];
        $foto = $_FILES["foto"];

        if(!empty($nome) && !empty($prezzo) && !empty($descrizione)) {
            list($result, $mess) = uploadImage("../img/", $_FILES["foto"]);
            if($result != 0){
                $foto = $mess;
                $result = $dbh->insertNuovoModello($nome, $descrizione, $_GET["store"], $quantità, $prezzo, $foto, $misura);
                if($result) {
                    //STAMPA MESSAGGIO DI SUCCESSO
                    $msg = "Prodotti aggiunti con successo!";
                } else {
                    //STAMPA MESSAGGIO DI FALLIMENTO
                    $msg = "Inserimento prodotti fallito :(";
                }
            }
        }
    } else {
        $msg = "Ricordati di inserire tutti i dati!";
    }
    $_SESSION["nuovoProdottoAggiunto"] = $msg;
    header("location: ../venditore.php?store=".$_GET["store"]);
?>