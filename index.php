<?php
require_once 'bootstrap.php';
if(isset($_GET["logout"])) {
    session_unset();
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
}

//Base Template
$templateParams["pagina"]="Home";
$templateParams["titolo"] = "La Rinascente Shoes & Shop online";
$templateParams["body"] = "template/home.php";

require 'template/baseBigHeader.php';
?>