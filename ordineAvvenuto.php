<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("location: index.php");
}
//Base Template
$templateParams["pagina"]="ordineAvvenuto";
$templateParams["body"] = "template/formOrdine.php";

require 'template/baseBigHeader.php';
?>