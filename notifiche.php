<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("location: index.php");
}
//Base Template
$templateParams["pagina"]="notifiche";
$templateParams["js"] = array("js/jquery-3.5.1.min.js","js/dynamicNotifiche.js");


require 'template/baseSmallHeaderBottoni.php';
?>