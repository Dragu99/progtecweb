<div class="row">
	<div class="col-md-1"></div>
	<div class="col-12 col-md-12">
		<header>
			<h2 class="px-5">Il tuo ordine è avvenuto con successo</h2>
			<p class="px-5">Grazie per aver acquistato prodotti Rinascente Shoes & Shop Online. Riceverai una notifica di acquisto avvenuto, per eventuali domande è possibile utilizzare i contatti sotto riportati </p>
        </header>
	</div> 
	<div class="col-md-1"></div>
</div> 
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-12 col-md-12">
		<ul class="nav nav-pills">
			<li class="nav-item col-6 col-md-6">
				<a class="nav-link shadow-lg mt-2 text-center text-white bg-dark" href="stilisti.php">Al negozio</a>
			</li>
			<li class="nav-item col-6 col-md-6">
				<a class="nav-link shadow-lg mt-2 text-center text-white bg-dark" href="index.php?logout"><em class="fas fa-share-square"></em>Logout</a>
			</li>
		</ul>
	</div>
	<div class="col-md-1"></div>
</div> 