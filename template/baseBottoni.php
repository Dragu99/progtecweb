<!doctype html>
<html lang="it">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
		<link rel="stylesheet" href="css/style.css" >
        <?php
        if(isset($templateParams["js"])):
            foreach($templateParams["js"] as $script):
        ?>
            <script src="<?php echo $script; ?>"></script>
        <?php
            endforeach;
        endif;
        ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <title>La Rinascente Shoes & Shop online - <?php echo $templateParams["titolo"]; ?></title>
	</head>
	<body>
        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title"><?php if(isset($templateParams["msg"])): echo $templateParams["msg"]; endif;?></h6>
                    </div>
                </div>
            </div>
        </div>
        <?php if(isset($templateParams["smallHeader"])): ?>
            <div class="container pt-3">
                <div class="row">
                    <div class="col-12">
                        <header class="text-center">
                            <h6>La Rinascente Shoes & Shop online</h6>
                        </header>
                    </div>   
                </div>
            </div> 
        <?php endif; ?>
        <div class="col-md-1"></div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <ul class="nav nav-pills justify-content-center">
                        <li class="nav-item col-4 px-1 px-md-3">
                            <a class="nav-link shadow-lg text-center text-white bg-dark px-1 px-md-3" href="notifiche.php">
                                <em class="fas fa-bell"></em> Notifiche</a>
                        </li>
                        <?php if(!isset($templateParams["venditore"])): ?>
                            <li class="nav-item col-4 px-1 px-md-3">
                                <a class="nav-link shadow-lg text-center text-white bg-dark px-1 px-md-3" href="carrello.php">
                                    <em class="fas fa-shopping-cart"></em> Carrello
                                    <span id="badge" class="badge badge-light align-text-top rounded-circle">
                                        <?php if(isset($_SESSION["productsInCart"])):
                                            echo $_SESSION["productsInCart"];
                                        endif;?>
                                    </span></a>
                            </li>
                        <?php endif; ?>
                        <li class="nav-item col-4 px-1 px-md-3">
                            <a id="logout" class="nav-link shadow-lg text-center text-white bg-dark px-1 px-md-3" href="index.php?logout">
                                <em class="fas fa-share-square"></em> Logout</a>
                        </li>
                    </ul>
                </div>
            </div> 
        </div>
        <div class="container p-2 my-2">
            <div class="row">
				<div class="col-12 col-md-12">
                    <main>
                        <header class="rounded-bottom border border-dark shadow-lg p-4 mb-4 text-center p-3"> 
                            <h1><?php echo $templateParams["header"]; ?></h1>
                            <?php if(isset($templateParams["venditore"])): ?>
                                <h4>Seller</h4>
                            <?php endif; ?>
                        </header> 
                    </main>
                </div>
            </div>
        </div>
		<div class="container p-2 my-2 bg-dark text-white">
			<div class="row">
				<div class="col-12">
					<footer class="text-center py-3 ">
						<p>Per maggiori informazioni
						<br>
						mail: laRinascenteShoes.Spa@gmail.com
						<br>
						sede centrale: Milano, Via Washington 70 
						</p>
					</footer>
				</div>
			</div>
		</div>
    </body>
</html>