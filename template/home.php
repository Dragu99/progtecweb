
<div class="row">
		<div class="col-md-1"></div>
		<div class="col-12 col-md-12">
			<ul class="nav nav-pills">
				<li class="nav-item col-6 col-md-6">
					<a class="nav-link shadow-lg mt-2 text-center text-white bg-dark" href="registrati.php">Registrati</a>
				</li>
				<li class="nav-item col-6 col-md-6">
					<a class="nav-link shadow-lg mt-2 text-center text-white bg-dark" href="login.php">Login</a>
				</li>
			</ul>
		</div>
		<div class="col-md-1"></div>
</div> 
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-12 col-md-12">
		<header class=" mt-4 mb-4">
			<div id="firstCarousel" class="carousel slide" data-interval="2000" data-ride="carousel">
				<ul class="carousel-indicators">
					<li data-target="#firstCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#firstCarousel" data-slide-to="1"></li>
					<li data-target="#firstCarousel" data-slide-to="2"></li>
					<li data-target="#firstCarousel" data-slide-to="3"></li>
				</ul>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img src="./img/sfilata1.jpg" class="mx-auto d-block" alt="" />
					</div>
					<div class="carousel-item">
						<img src="./img/sfilata2.jpg" class="mx-auto d-block" alt="" />
					</div>
					<div class="carousel-item">
						<img src="./img/sfilata3.jpg" class="mx-auto d-block" alt="" />
					</div>
					<div class="carousel-item">
						<img src="./img/sfilata4.jpg" class="mx-auto d-block" alt="" />
					</div>
				</div>
				<a class="carousel-control-prev" href="#firstCarousel" data-slide="prev">
					<span class="carousel-control-prev-icon"></span>
				</a>
				<a class="carousel-control-next" href="#firstCarousel" data-slide="next">
					<span class="carousel-control-next-icon"></span>
				</a>
			</div>
			<h2 class="px-5 text-center">Lo shopping di alta moda a casa tua</h2>
			<p class="px-5 text-center">Stagione Primavera-Estate 2021</p>
		</header>
		<section class="px-5 text-center">
			<p>Scopri le nuove collezioni di calzature, brand dedicati a te e tutte le proposte degli stilisti di alta moda</p>
		</section>
	</div> 
	<div class="col-md-1"></div>
</div> 