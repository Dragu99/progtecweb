<section id="sectionLogin" class="px-5">
	<h2>Registrati</h2>
	<form action="#" method="POST">
		<ul class="nav nav-pills">
			<li class="nav-item col-12 col-md-12">
				<label for="name">Nome</label>
				<input type="text" class="form-control form-control-sm" placeholder="Andrea" id="name" name="name"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="surname">Cognome</label>
				<input type="text" class="form-control form-control-sm" placeholder="Bianchi" id="surname" name="surname"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="email">E-mail</label>
				<input type="text" class="form-control form-control-sm" placeholder="andrea.bianchi@gmail.com" id="email" name="email"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="password">Password</label>
				<input type="password" class="form-control form-control-sm" id="password" name="password"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="confirmation">Conferma password</label>
				<input type="password" class="form-control form-control-sm" id="confirmation" name="confirmation"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<input type="submit" class="nav-link shadow-lg mt-2 text-center text-white bg-dark col-12 col-md-12" name="submitbutton" value="Conferma" >
			</li>
		</ul>
		<ul class="nav nav-pills">
			<li class="nav-item col-12 col-md-12">
				<p>Sei già registrato?<a href="login.php">Clicca qui</a></p>	
			</li>
		</ul>
	</form>
</section>