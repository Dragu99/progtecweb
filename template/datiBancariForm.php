<section id="sectionLogin" class="px-5">
	<h2>Inserisci i tuoi dati</h2>
	<form action="./process/processVendita.php" method="POST">
		<ul class="nav nav-pills">
			<li class="nav-item col-12 col-md-12">
				<label for="cardNumber">Numero di carta</label>
				<input type="number" class="form-control form-control-sm" placeholder="0123456789012345" id="cardNumber" name="cardNumber"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="cardCode">Codice di sicurezza</label>
				<input type="password" class="form-control form-control-sm"  placeholder="012" id="cardCode" name="cardCode"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="expirationDate">Data di scadenza</label>
				<input type="month" class="form-control form-control-sm" id="expirationDate" name="expirationDate"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="address">Indirizzo</label>
				<input type="text" class="form-control form-control-sm"  placeholder="via Garibaldi n.13" id="address" name="address"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="city">Comune</label>
				<input type="text" class="form-control form-control-sm"  placeholder="Milano" id="city" name="city"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="cap">CAP</label>
				<input type="number" class="form-control form-control-sm"  placeholder="20122" id="cap" name="cap"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<input type="submit" class="nav-link shadow-lg mt-2 text-center text-white bg-dark col-12 col-md-12" name="submitbutton" value="Conferma" >
			</li>
		</ul>
	</form>
 </section>