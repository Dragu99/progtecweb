<!doctype html>
<html lang="it-IT">
	<head>
		<?php
			if(isset($templateParams["js"])):
				foreach($templateParams["js"] as $script):
		?>
            <script src="<?php echo $script; ?>"></script>
        <?php
				endforeach;
			endif;
        ?>
	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

		<link rel="stylesheet" href="css/style.css" >

		<title>La Rinascente Shoes & Shop online - Registrati</title>
	</head>
	<body id="bodyLogin">
		<div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title"></h6>
                    </div>
                </div>
            </div>
        </div>
		<div class="container pt-3">
			<div id="divLogin" class="container">
				<div class="row">
					<div class="col-12">
						<header class="text-center">
							<p><small>La Rinascente Shoes & Shop online</small></p>
						</header>
					</div>   
				</div>
				<main>
				<?php
					if(isset($templateParams["body"])):
						require($templateParams["body"]);
					endif;
				?>
				</main>
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-12 col-md-12">
						<ul class="nav nav-pills">
							<li class="nav-item col-6 col-md-6">
								<a class="nav-link shadow-lg mt-2 text-center text-white bg-dark" href="
								<?php if(isset($_SESSION["venditore"])):
									echo "venditore.php?store=".$_SESSION["venditore"];
								else: echo "stilisti.php";
								endif;?>">Al negozio</a>
							</li>
							<li class="nav-item col-6 col-md-6">
								<a class="nav-link shadow-lg mt-2 text-center text-white bg-dark" href="index.php?logout"><em class="fas fa-share-square"></em>Logout</a>
							</li>
						</ul>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
			<div class="container p-2 my-2 bg-dark text-white">
				<div class="row">
					<div class="col-12">
						<footer class="text-center py-3">
							<p>Per maggiori informazioni
							<br>
							mail: laRinascenteShoes.Spa@gmail.com
							<br>
							sede centrale: Milano, Via Washington 70 
							</p>
						</footer>
					</div>
				</div>
			</div>
		</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
