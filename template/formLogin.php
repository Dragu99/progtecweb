<section id="sectionLogin" class="px-5">
	<h2>Login</h2>
	<?php if(isset($templateParams["errorelogin"])): ?>
	<p><?php echo $templateParams["errorelogin"];?></p>
	<?php endif; ?>
	<form action="#" method="POST">
		<ul class="nav nav-pills">
			<li class="nav-item col-12 col-md-12">
				<label for="email">E-mail</label>
				<input type="text" class="form-control form-control-sm" placeholder="andrea.bianchi@gmail.com" id="email" name="email"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<label for="password">Password</label>
				<input type="password" class="form-control form-control-sm" id="password" name="password"/>
			</li>
			<li class="nav-item col-12 col-md-12">
				<input type="submit" class="nav-link shadow-lg mt-2 text-center text-white bg-dark col-12 col-md-12" name="submitbutton" value="Conferma" >
			</li>
		</ul>
		<ul class="nav nav-pills">
			<li class="nav-item col-12 col-md-12">
				<p>Non sei ancora registrato?<a href="registrati.php">Clicca qui</a></p>
			</li>
		</ul>
	</form>
</section>