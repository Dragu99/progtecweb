function generaStores(stores){
    let start = `<ul class="nav d-flex justify-content-around">`;
    let end = `</ul>`;
    let result = "";

    for(let i=0; i < stores.length; i++){
        let store = `
            <li class="nav-item col-12 col-md-6">
                <a class="nav-link d-flex justify-content-around align-items-center shadow-lg text-center rounded text-white bg-dark" href="shopping.php?store=${stores[i]["marchio"]}">
                    <img src="${stores[i]["img"]}" class="rounded float-left w-25" alt="Immagine relativa al prodotto"/>
                    <p class="flex-grow-1">${stores[i]["marchio"]}</p>
                </a>
            </li>`;
        result += store;
    }
    return start.concat(result).concat(end);
}

$(document).ready(function(){
    $.getJSON("./api/api-stores.php", function(data){
        let stores = generaStores(data);
        $("main").append(stores);
    });
});