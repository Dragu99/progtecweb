const pattern = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;
const re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;

$(document).ready(function(){
    $(document).on("submit", "form", function(event){
        event.preventDefault();
        let isFormOk = true;
        let msg = ``;
        
		
        const name = $("input#name");
		$("input#name + p").remove();
        if(name.val()==undefined || name.val().length <2 || name.val().length>80){
            msg += `Formato nome sbagliato<br/>`;
            isFormOk = false;
        }
		
		const surname = $("input#surname");
		$("input#surname + p").remove();
        if(surname.val()==undefined || surname.val().length <2 || surname.val().length>80){
            msg += `Formato cognome sbagliato<br/>`;
            isFormOk = false;
        }
		
		const email = $("input#email");
		$("input#email + p").remove();
        if(!re.test(email.val())){
			msg += `Formato e-mail sbagliato<br/>`;
            isFormOk = false;
		}
		
        const password = $("input#password");
        $("input#password + p").remove();
        if(password.val()==undefined || password.val().length<8 || password.val().length>64){
            msg += `Password deve essere almeno 8 caratteri e deve contenere almeno una lettera ed un numero<br/>`;
            isFormOk = false;
        }

		const confirmation = $("input#confirmation");
		$("input#confirmation + p").remove();
		if(confirmation.val()==undefined || confirmation.val()!= password.val()){
			msg += `La password non corrisponde a quella inserita<br/>`;
			isFormOk = false;
        }

        if(!isFormOk) {
            $(".modal-title").empty();
            $(".modal-title").append(msg);
            $('#modal').modal()
        } else {
            var serializedData = $(this).serialize();
            $.post("./process/processRegistrazione.php", serializedData, function(response) {
                if(response !== "logged"){
                    $(".modal-title").empty();
                    $(".modal-title").append(response);
                    $('#modal').modal();
                } else {
                    event.currentTarget.submit();
                    window.location.href = "stilisti.php";
                }
            });
        }

    });
	
    
});