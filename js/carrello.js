function generaCarrello(carrello, prodotti){
    let start = `<section id="section" class="d-flex justify-content-around">
					<h1>Il mio carrello</h1>
				</section>
				<ul class="nav d-flex justify-content-around">`;
    let result = "";
    for(let codModello in carrello){
		let dettagliProdotto = getDettagliProdotto(codModello, prodotti);
		for(let misura in carrello[codModello]) {
			importi[codModello] = dettagliProdotto["prezzo"];
			quantità[codModello] = carrello[codModello][misura];
			importotot += dettagliProdotto["prezzo"]*carrello[codModello][misura];
			let prodotto = `<li id="${codModello}" class="nav-item col-md-6 px-md-2">
								<article class="bg-dark text-white rounded d-flex flex-column p-2">
										<form id="form" method="post">
											<div class="d-flex justify-content-around align-items-center">
												<img src="${dettagliProdotto["foto"]}" class="rounded float-left" alt="immagine illustrativa del prodotto"/>
													<div class="flex-column flex-grow-1">
														<div class="d-flex flex-column justify-content-around align-items-baseline flex-wrap ml-2">
															<input type="hidden" value="${codModello}" name="codModello"/>
															<input type="hidden" value="${misura}" name="misura"/>
															<h5>${dettagliProdotto["nome"]}</h5>
															<output id="prezzo" value="${dettagliProdotto["prezzo"]}.00">${dettagliProdotto["prezzo"]},00 €</output>
																<label for="misura" class="m-0">Misura: <output name="mis" value="${misura}"> ${misura}</output></label>
															<h6 id="quant">Quantità: ${carrello[codModello][misura]}</h6>
														</div>
													</div>
											</div>
											<div class="d-flex justify-content-end">
													<button class="btn bg-white border" form="form"> Rimuovi </button>
												</div>
										</form>
								</article>
							</li>`;
			result += prodotto;
		}
	}
	let end = `	</ul>
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-12 col-md-12">
						<label for="importo totale">Importo totale: </label>
						<output id="imptot" name="imptot" value="${importotot}.00">${importotot},00 €</output>
						<form id="formSconto" class="form-inline" method="POST">
							<input type="hidden" value="${importotot}.00" name="importo"/>
							<label for="codice sconto">Codice Sconto: </label>
							<div class="d-flex">
								<input type="text" class="form-control form-control-sm" placeholder="Inserisci codice" id="codSconto" name="codSconto">
								<button type="submit" class="btn btn-sm bg-white" id="riscuoti" name="btnRiscuoti" value="Riscuoti">Riscuoti</button>
							</div>
							<button type="submit" class="btn btn-block shadow-lg my-2 text-center text-white bg-dark" id="conferma" name="btnConferma" value="Conferma">Conferma ordine</button>
						</form>
					<div class="col-md-1"></div>
					</div>
				</div>`;
    return start.concat(result).concat(end);
}

function getDettagliProdotto(codModello, prodotti){
	let result = "";
	for (let i = 0; i < prodotti.length; i++) {
		if (prodotti[i]["codModello"] == codModello) {
			result = prodotti[i];
		}
	}
	return result;
}

let importotot = 0;
let importi = {};
let quantità = {};

$(document).ready(function(){
    $.getJSON("./api/api-carrello.php", function(data){
		let carrello = data;
		$.getJSON("./api/api-prodottishopping.php", function(data){
			let prodottiNelCarrello = generaCarrello(carrello, data);
			$("main").append(prodottiNelCarrello);
		});
	});

	$(document).on("click", "#riscuoti, #conferma", function(event){
		console.log($(event.target).attr("id"));
		btn = $(event.target).attr("id");
		$("#formSconto").submit(function(e) {
			e.preventDefault();
			var serializedData = $(this).serialize();
			if(importotot>0){
				$.post("./process/processSconto.php?btn=".concat(btn), serializedData, function(response) {
					if(response === "Ci dispiace ma non puoi usufruire di questo sconto"){
						$(".modal-title").text(response);
						$('#modal').modal();
					} else if (response === "datiBancari.php") {
						window.location.href = response;
					} else if (response === "0") {
						//non succede nulla
					} else {
						importotot = response;
						$("#imptot").text(response.concat(",00 €"));
					}
				});
			}
		});

	});

	$(document).on("submit", "#form", function(event){
		event.preventDefault();
        var serializedData = $(this).serialize();

        $.post("./process/processRimozione.php?", serializedData, function(response) {
			quantità[response] -= 1;
            if(quantità[response] == 0){
				$('#'.concat(response)).remove();
			} else {
				$("#quant").text("Quantità: ".concat(quantità[response]));
			}
			importotot -= importi[response];
			$("#imptot").text(String(importotot).concat(",00 €"));
        });
	});
});