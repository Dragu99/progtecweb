const pattern = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;
$(document).ready(function(){
    $(document).on("submit", "form", function(event){
        event.preventDefault();
        let isFormOk = true;
        let msg = ``;
		
        const cardNumber = $("input#cardNumber");
		$("input#cardNumber + p").remove();
        if(cardNumber.val()==undefined || cardNumber.val().length!=16 ){
            msg += `Formato numero carta sbagliato<br/>`;
            isFormOk = false;
        }
		
		const cardCode = $("input#cardCode");
		$("input#cardCode + p").remove();
        if(cardCode.val()==undefined || cardCode.val().length!=3){
            msg += `Formato codice sicurezza carta sbagliato<br/>`;
            isFormOk = false;
        }
		
		const expirationDate = $("input#expirationDate");
		$("input#expirationDate + p").remove();
		let today = new Date();
		let data = new Date($("input#expirationDate").val());
		let month = data.getMonth()+1;
		let year = data.getFullYear();
		let yearNow = today.getFullYear();
		let monthNow = today.getMonth()+1;
		if(data=="Invalid Date" || year<yearNow || (year==yearNow && month<monthNow)){
			msg += `Carta scaduta o data non valida<br/>`;
            isFormOk = false;
        }
		
		const address = $("input#address");
		$("input#address + p").remove();
        if(address.val()==undefined || address.val().length <5 || address.val().length>80){
            msg += `Formato indirizzo sbagliato<br/>`;
            isFormOk = false;
        }
		
		const city = $("input#city");
		$("input#city + p").remove();
        if(city.val()==undefined || city.val().length <5 || city.val().length>80){
            msg += `Formato comune sbagliato<br/>`;
            isFormOk = false;
        }
		
		const cap = $("input#cap");
		$("input#cap + p").remove();
        if(cap.val()==undefined || cap.val().length!=5){
            msg += `Formato CAP sbagliato<br/>`;
            isFormOk = false;
        }


        if(!isFormOk) {
            $(".modal-title").empty();
            $(".modal-title").append(msg);
            $('#modal').modal()
        } else {
            var serializedData = $(this).serialize();
            $.post("./process/processAcquisto.php", serializedData, function(response) {
                if(response !== "success"){
                    $(".modal-title").empty();
                    $(".modal-title").append(response);
                    $('#modal').modal();
                } else {
                    event.currentTarget.submit();
                    window.location.href = "ordineAvvenuto.php";
                }
            });
        }

    });
    
});