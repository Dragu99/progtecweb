function generaNotifiche(notifiche){
    let start = `<section  id="section" class="px-1">
						<h1 class="text-center flex-wrap">Benvenuto nel tuo spazio notifiche </h1>
						<h5 class="text-center">${notifiche[0]["destinatario"]}</h5>
						<ul class="list-group">`;
    let end = `	</ul>
					</section>`;
    let result = "";

    for(let i=0; i < notifiche.length; i++){
		id = notifiche[i]["codNotifica"];
		let date = notifiche[i]["data"].split(' ')[0];
        let notifica = `<div id="accordion">
							<div class="card mt-2">
								<div class="card-header">
									<a class="card-link d-flex flex-wrap justify-content-between" data-toggle="collapse" href="#acc${id}">
										<p><b>${notifiche[i]["oggetto"]}</b></p>
										<p class="ml-auto"><b>${date}</b></p>
									</a>
								</div>
								<div id="acc${id}" class="collapse" data-parent="#accordion">
									<div class="card-body">
										${notifiche[i]["testo"]}
									</div>
								</div>
							</div>
						</div>`;
        result += notifica;
    }
    return start.concat(result).concat(end);
}

$(document).ready(function(){
    $.getJSON("./api/api-notifiche.php", function(data){
        let notifiche = generaNotifiche(data);
        $("main").append(notifiche);
    });
});