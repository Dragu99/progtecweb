function generaAltriStores(stores) {
    let start = `<ul class="nav nav-pills">`;
    let end = `</ul>`;
    let result = "";

    for(let i=0; i < stores.length; i++) {
        let store = `<li class="nav-item p-0 col-3 col-md-3">
                        <a class="nav-link p-0 mt-0 d-flex flex-column align-items-center" href="shopping.php?store=${stores[i]["marchio"]}">
                            <img src="${stores[i]["img"]}" class="rounded float-left w-75" alt="Al negozio ${stores[i]["marchio"]}"/>
                            <!-- <h6 class="text-dark">Yves Saint Laurent</h6> -->
                        </a>
                    </li>`;
        result += store;
    }
    return start.concat(result).concat(end);
}

function generaListaProdottiShopping(prodotti, misure){
    let start = `<div class="nav d-flex justify-content-around flex-wrap">`;
    let end = `</div>`;
    let result = "";

    for(let i=0; i < prodotti.length; i++){
        let prodottoPt1 = `
                <article class="nav-item col-md-10 bg-dark text-white rounded d-flex flex-column p-2 my-2">
                    <div class="d-flex p-2 justify-content-around align-items-center">
                        <img src="${prodotti[i]["foto"]}" class="rounded float-left" alt="${prodotti[i]["descrizione"]}"/>
                        <div class="flex-column flex-grow-1">
                            <form method="post">
                                <div class="d-flex justify-content-around align-items-baseline flex-wrap">
                                    <h4>${prodotti[i]["nome"]}</h4>
                                    <h5>€ ${prodotti[i]["prezzo"]},00</h5>
									<label for="misura" hidden>Seleziona la misura</label>
                                </div>
                                <div class="d-flex justify-content-around">
                                    <input type="hidden" name="codModello" value="${prodotti[i]["codModello"]}">
									
                                        <select name="misura" class="btn bg-white selectpicker">
                                            <option selected="selected">Misura</option>`;
        let prodottoPt2 = `             </select>
                                    <button type="submit" class="btn bg-white" name="add to cart"><em class="fas fa-cart-plus"></em></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <p class="pt-2 px-2 mb-0">${prodotti[i]["descrizione"]}</p>
                </article>`;
        let misureProd = generaMisure(prodotti[i]["codModello"], misure);
        prodotto = prodottoPt1.concat(misureProd).concat(prodottoPt2);
        result += prodotto;
    }
    return start.concat(result).concat(end);
}

function generaMisure(codModello, misure) {
    let result = "";

    for(let i=0; i < misure.length; i++) {
        if(misure[i]["codModello"] == codModello) {
            let misura = `<option>${misure[i]["misura"]}</option>`;
            result += misura;
        }
    }
    return result;
}

$(document).ready(function(){
    const store = $("h1").text();
    $.getJSON("./api/api-stores.php?store=".concat(store), function(data){
            let stores = generaAltriStores(data);
            $("main").append(stores);
    });
    $.getJSON("./api/api-misure.php?store=".concat(store), function(data) {
        let misure = data;
        $.getJSON("./api/api-prodottishopping.php?store=".concat(store), function(data){
            let prodotti = generaListaProdottiShopping(data, misure);
            $("main").append(prodotti);
        });
    });

    $(document).on("submit", "form", function(event){
        event.preventDefault();
        var serializedData = $(this).serialize();

        $.post("./process/processCart.php?store=".concat(store), serializedData, function(response) {
            // Log the response to the console
            if(response["msg"] !== ""){
                $(".modal-title").text(response["msg"]);
                $('#modal').modal();
            }
            $("select").val("Misura");
            if(response["productsInCart"] !== "undefined") {
                $("#badge").text(response["productsInCart"]);
            }
        });
    });    
});

