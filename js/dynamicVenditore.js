/*
*/

function generaListaProdottiShopping(prodotti, misure, store){
    let start = `<div class="d-flex col-md-12 px-md-2 p-0 mx-0 justify-content-around">
    <article class="bg-dark text-white rounded p-2">
        <div class="d-flex flex-column justify-content-around align-items-center px-1">
            <h4>Inserisci nuovo prodotto</h4>
            <form id="formNuovoProdotto" action="./process/processNuovoProdotto.php?store=${store}" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="nome" class="mr-2 mr-sm-2">Nome: <input type="text" class="form-control" id="nome" name="nome"/></label>
                    <label for="quantità">Quantità: <input type="number" id="quantità" name="quantità" value="1" min="1" max="10" class="form-control"/></label>
                </div>
                    <div class="d-flex justify-content-between">
                        <div class="form-group">
                            <label for="prezzo" class="mr-2 mr-sm-2">Prezzo: </label>
                            <div class="input-group mb-3">
                                <input type="number" id="prezzo" name="prezzo" value="250" min="1" max="5000" class="form-control text-right"/>
                                <div class="input-group-append w-25">
                                    <span class="input-group-text">,00 €</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="misura">Misura: </label>
                            <input type="number" id="misura" name="misura" value="36" min="36" max="41" class="form-control"/>
                        </div>
                    </div>
                <div class="form-group">
                    <label for="descrizione">Descrizione:</label>
                    <textarea class="form-control" id="descrizione" name="descrizione" rows="2"></textarea>
                </div>
                <div class="form-group">
                    <label for="foto">Foto:</label>
                    <input type="file" id="foto" name="foto">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block mt-3 mb-0 bg-white" name="inserisci">Conferma</button>
                </div>
            </form>
        </div>
    </article>
</div>
                <ul class="nav d-flex justify-content-around">`;
    let end = `</ul>`;
    let result = "";

    for(let i=0; i < prodotti.length; i++){
        let prodottoPt1 = `<li class="nav-item col-md-6 px-md-2">
                <article class="bg-dark text-white rounded d-flex flex-column p-2">
                    <div class="d-flex justify-content-around align-items-center">
                        <img src="${prodotti[i]["foto"]}" class="rounded float-left" alt="${prodotti[i]["descrizione"]}"/>
                        <div class="flex-column flex-grow-1">
                            <form id="formProd" method="post">
                                <div class="d-flex justify-content-around align-items-baseline mb-3">
                                    <h4 class="d-flex justify-content-start align-items-baseline"">${prodotti[i]["nome"]}</h4>
                                    <button type="submit" class="btn bg-white" name="addtocart"><em class="fas fa-plus-square"></em></button>
									<label for="misura" hidden>Seleziona la misura</label>
                                </div>
                                <div class="d-flex flex-wrap justify-content-around align-content-around">
                                    <input type="hidden" name="codModello" value="${prodotti[i]["codModello"]}">
                                        <select class="btn bg-white selectpicker" name="misura">
                                            <option selected="selected">Misura</option>`;
        let prodottoPt2 = `             </select>
                                    <div class="d-flex">
                                        <label hidden for="quantità" class="input-number-label"></label>
                                            <input type="number" class="rounded" id="quantità" name="quantità" value="1" min="1" max="10">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </article>
                </li>`;
        let misureProd = generaMisure(prodotti[i]["codModello"], misure);
        prodotto = prodottoPt1.concat(misureProd).concat(prodottoPt2);
        result += prodotto;
    }
    return start.concat(result).concat(end);
}

function generaMisure(codModello, misure) {
    let result = "";

    for(let i=0; i < misure.length; i++) {
        if(misure[i]["codModello"] == codModello) {
            let misura = `<option>${misure[i]["misura"]}</option>`;
            result += misura;
        }
    }
    return result;
}

$(document).ready(function(){
    const store = $("h1").text();
    $.getJSON("./api/api-misure.php?store=".concat(store), function(data) {
        let misure = data;
        $.getJSON("./api/api-prodottishopping.php?store=".concat(store), function(data){
            let prodotti = generaListaProdottiShopping(data, misure, store);
            $("main").append(prodotti);
        });
    });

    console.log($('.modal-title').text());

    if($('.modal-title').text() != "") {
        $('#modal').modal();
    }

    $(document).on("submit", "#formNuovoProdotto", function(event){
        event.preventDefault();
        let isFormOk = true;
        let msg = ``;
        
        
        const name = $("input#nome");
        $("input#nome + p").remove();
        if(name.val()==undefined || name.val().length <2 || name.val().length>100){
            msg += `Formato nome sbagliato<br/>`;
            isFormOk = false;
        }
        
        const desc = $("textarea#descrizione");
        $("textarea#descrizione + p").remove();
        if(desc.val()==undefined || desc.val().length<2 || desc.val().length>254){
            msg += `Formato descrizione sbagliato<br/>`;
            isFormOk = false;
        }

        if ($('#foto').get(0).files.length === 0) {
            msg += `Non hai selezionato un'immagine<br/>`;
            isFormOk = false;
        }

        if(!isFormOk) {
            $(".modal-title").empty();
            $(".modal-title").append(msg);
            $('#modal').modal()
        } else {
            event.currentTarget.submit();
        }
    });

    $(document).on("submit", "#formProd", function(event){
        event.preventDefault();
        var serializedData = $(this).serialize();

        $.post("./process/processSellerCart.php?store=".concat(store), serializedData, function(response) {
            if(response !== ""){
                $(".modal-title").empty();
                $(".modal-title").text(response);
                $('#modal').modal();
            }
            $("select").val("Misura");
            $("#quantità").val("1");
        });
    });
});

