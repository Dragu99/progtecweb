const pattern = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;

$(document).ready(function(){
    $(document).on("submit", "form", function(event){
        event.preventDefault();
        let isFormOk = true;
        let msg = ``;
		
        const email = $("input#email");
		$("input#email + p").remove();
        if(email.val()==undefined || email.val().length <5 || email.val().length>80){
            msg += `Formato e-mail sbagliato`;
            isFormOk = false;
        }
		
        const password = $("input#password");
        $("input#password + p").remove();
        if(password.val()==undefined || password.val().length<8 || password.val().length>64){
            msg += `Password deve essere almeno 8 caratteri e deve contenere almeno una lettera ed un numero`;
            isFormOk = false;
        }
        
        if(!isFormOk) {
            $(".modal-title").empty();
            $(".modal-title").append(msg);
            $('#modal').modal()
        } else {
            var serializedData = $(this).serialize();
            $.post("./process/processLogin.php", serializedData, function(response) {
                console.log(response);
                if(response === "Errore! Username o password non corretti"){
                    $(".modal-title").empty();
                    $(".modal-title").append(response);
                    $('#modal').modal();
                } else if(response === "utente") {
                    event.currentTarget.submit();
                    window.location.href = "stilisti.php";
                } else {
                    event.currentTarget.submit();
                    window.location.href = "venditore.php?store=".concat(response);
                }
            });
        }
    });
});