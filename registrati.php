<?php
require_once 'bootstrap.php';
if(isUserLoggedIn()){
    header("location: stilisti.php");
}

//Base Template
$templateParams["pagina"]="registrati";
$templateParams["body"] = "template/formRegistrati.php";
$templateParams["js"] = array("js/jquery-3.5.1.min.js","js/controlsRegistrati.js");

require 'template/baseSmallHeader.php';
?>