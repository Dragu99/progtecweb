<?php
require_once 'bootstrap.php';
if(isUserLoggedIn()){
    header("location: stilisti.php");
}

$templateParams["pagina"] = "login";
$templateParams["body"] = "template/formLogin.php";
$templateParams["js"] = array("js/jquery-3.5.1.min.js","js/controlsLogin.js");

require 'template/baseSmallHeader.php';
?>