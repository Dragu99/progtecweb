<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }

public function getShoppingProducts($store) {
    $stmt = $this->db->prepare("SELECT m.* FROM modello AS m, versione_modello AS vm WHERE m.codModello=vm.codModello AND m.marchio=? AND vm.quantità > 0 GROUP BY m.codModello");
    $stmt->bind_param('s', $store);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}


   


public function getItem($codModello) {
    $stmt = $this->db->prepare("SELECT * FROM modello WHERE codModello=?");
    $stmt->bind_param('s', $codModello);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}

public function getAllShoppingProducts() {
    $stmt = $this->db->prepare("SELECT * FROM modello");
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}

public function getUserNotification($user) {
	$stmt = $this->db->prepare("SELECT * FROM notifica WHERE destinatario=? ORDER by data DESC");
    $stmt->bind_param('s', $user);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}

public function getMisureProdotti($store) {
    $stmt = $this->db->prepare("SELECT vm.codModello, vm.misura FROM versione_modello AS vm, modello AS m WHERE m.codModello=vm.codModello AND m.marchio=? AND vm.quantità > 0");
    $stmt->bind_param('s', $store);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}

public function getStores($store="nessuno") {
    $stmt = $this->db->prepare("SELECT marchio, img FROM persona WHERE marchio!=?");
    $stmt->bind_param('s', $store);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}

public function updateDisponibilitàProdotto($codModello, $misura, $quantità){
    $query = "UPDATE versione_modello SET quantità = ? WHERE codModello = ? AND misura = ?";
    $stmt = $this->db->prepare($query);
    $prod = $this->getDisponibilitàProdotto($codModello, $misura);
    $nuovaQuantità = (int)$prod[0]["quantità"]+(int)($quantità);
    $stmt->bind_param('isi', $nuovaQuantità, $codModello, $misura);
    
    return $stmt->execute();
}

public function getDisponibilitàProdotto($codModello, $misura){
    $stmt = $this->db->prepare("SELECT quantità FROM versione_modello WHERE codModello = ? AND misura = ?");
    $stmt->bind_param('si',$codModello, $misura);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}

public function getShoppingManager($codModello){
    $query = "SELECT e_mail FROM persona AS p, modello AS m WHERE p.marchio=m.marchio AND m.codModello = ?";
    $stmt = $this->db->prepare($query);
    $stmt->bind_param('s', $codModello);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}

public function getUltimoAcquisto($email){
    $query = "SELECT data FROM acquisto WHERE e_mail = ? ORDER BY data DESC LIMIT 1";
    $stmt = $this->db->prepare($query);
    $stmt->bind_param('s', $email);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}

public function insertUtente($name, $surname, $email, $password){
    $hashedPwdInDb = password_hash($password, PASSWORD_DEFAULT);
    $query = "INSERT INTO persona (nome, cognome, e_mail, password) VALUES (?, ?, ?, ?)";
    $stmt = $this->db->prepare($query);
    $stmt->bind_param('ssss',$name, $surname, $email, $hashedPwdInDb);
    
    return $stmt->execute();
}

public function insertAcquisto($email, $importo, $sconto=NULL) {
	$query = "INSERT INTO acquisto (e_mail, prezzo_tot, sconto) VALUES (?, ?, ?)";
    $stmt = $this->db->prepare($query);
    $stmt->bind_param('sds',$email, $importo, $sconto);
    if($stmt->execute()) {
        $codOrdine = $stmt->insert_id;
		foreach($_SESSION["cart"] as $codModello => $misure) {
            foreach($misure as $misura => $quantità) {
                $res = $this->insertOrdine($codOrdine, $codModello, $misura, $quantità);
                if(!$res) {
                    return 0;
                } else {
                    $res2 = $this->updateDisponibilitàProdotto($codModello, $misura, (-$quantità));
                    if(!$res2) {
                        return 0;
                    }
                }
            }
		}
	}
	return $codOrdine;
}

public function insertOrdine($codOrdine, $codModello, $misura, $quantità){
	$query = "INSERT INTO ordine (codOrdine, codModello, misura, quantità) VALUES (?, ?, ?, ?)";
	$stmt = $this->db->prepare($query);
    $stmt->bind_param('isii', $codOrdine, $codModello, $misura, $quantità);
    
	return $stmt->execute();      
}

public function insertNotifica($destinatario, $oggetto, $testo, $codOrdine=NULL, $codSconto=NULL) {
	$query = "INSERT INTO notifica (destinatario, oggetto, testo, codOrdine, codSconto) VALUES (?, ?, ?, ?, ?)";
    $stmt = $this->db->prepare($query);
    $stmt->bind_param('sssis', $destinatario, $oggetto, $testo, $codOrdine, $codSconto);
	
	return $stmt->execute();
}

public function insertNuovoModello($nome, $descrizione, $marchio, $quantità, $prezzo, $foto, $misura) {
    $numRows = $this->getNumProdotti($marchio);
    $next = $numRows[0]["numRows"];
    $next++;
    $codModello = $next.$this->getLetter($marchio);
    $query = "INSERT INTO modello (codModello, nome, descrizione, foto, prezzo, marchio) VALUES (?, ?, ?, ?, ?, ?)";
    $stmt = $this->db->prepare($query);
    $stmt->bind_param('ssssds', $codModello, $nome, $descrizione, $foto, $prezzo, $marchio);
    $result = $stmt->execute();
    if($result) {
        $result = $this->insertVersioneModello($codModello, $misura, $quantità);
    }	
	return $result;
}

private function insertVersioneModello($codModello, $misura, $quantità) {
    $query = "INSERT INTO versione_modello (codModello, misura, quantità) VALUES (?, ?, ?)";
    $stmt = $this->db->prepare($query);
    $stmt->bind_param('sii', $codModello, $misura, $quantità);
    return $stmt->execute();
}

private function getLetter($marchio) {
    $letter = "";
    switch($marchio) {
        case "Giorgio Armani":
            $letter = "a";
            break;
        case "Chanel":
            $letter = "c";
            break;
        case "Michael Kors":
            $letter = "m";
            break;
        case "Louboutin":
            $letter = "l";
            break;
        case "Yves Saint Laurent":
            $letter = "y";
            break;
    }
    return $letter;
}

private function getNumProdotti($marchio) {
    $stmt = $this->db->prepare("SELECT COUNT(codModello) AS numRows FROM `modello` WHERE marchio=?");
    $stmt->bind_param("s", $marchio);
    $stmt->execute();
    $result = $stmt->get_result();
	
	return $result->fetch_all(MYSQLI_ASSOC);
}

public function checkCodiceSconto($email, $codSconto) {
    $stmt = $this->db->prepare("SELECT * FROM notifica WHERE destinatario = ? AND codSconto = ?");
    $stmt->bind_param("ss", $email, $codSconto);
    $stmt->execute();
    $result = $stmt->get_result();
	
	return $result->fetch_all(MYSQLI_ASSOC);
}

public function getSconto($codSconto) {
    $stmt = $this->db->prepare("SELECT percentuale FROM sconto WHERE codSconto = ?");
    $stmt->bind_param("s", $codSconto);
    $stmt->execute();
    $result = $stmt->get_result();
	
	return $result->fetch_all(MYSQLI_ASSOC);
}

public function checkEmail($email){
	$stmt = $this->db->prepare("SELECT * FROM persona WHERE e_mail = ?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $result = $stmt->get_result();
	
	return $result->fetch_all(MYSQLI_ASSOC);
}

 public function checkLogin($email, $password){
		$email_result = $this->checkEmail($email);
		if (count($email_result)!=0){
			$password_result = $this->getPassword($email_result[0]["e_mail"]);
            $hashedPwdInDb = $password_result[0]["password"];
			if(password_verify($password, $hashedPwdInDb)){
				return $email_result;
            } else {
                return $this->checkEmail("nada");
            }
		}else{
			return $email_result;
		}
}

private function getPassword($email) {
    $stmt = $this->db->prepare("SELECT password FROM persona WHERE e_mail = ?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
}

    /*ESEMPI A CUI ISPIRARSI

    public function getRandomPosts($n){
        $stmt = $this->db->prepare("SELECT idarticolo, titoloarticolo, imgarticolo FROM articolo ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertArticle($titoloarticolo, $testoarticolo, $anteprimaarticolo, $dataarticolo, $imgarticolo, $autore){
        $query = "INSERT INTO articolo (titoloarticolo, testoarticolo, anteprimaarticolo, dataarticolo, imgarticolo, autore) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssi',$titoloarticolo, $testoarticolo, $anteprimaarticolo, $dataarticolo, $imgarticolo, $autore);
        $stmt->execute();
        
        return $stmt->insert_id;
    }

    public function updateArticleOfAuthor($idarticolo, $titoloarticolo, $testoarticolo, $anteprimaarticolo, $imgarticolo, $autore){
        $query = "UPDATE articolo SET titoloarticolo = ?, testoarticolo = ?, anteprimaarticolo = ?, imgarticolo = ? WHERE idarticolo = ? AND autore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssii',$titoloarticolo, $testoarticolo, $anteprimaarticolo, $imgarticolo, $idarticolo, $autore);
        
        return $stmt->execute();
    }

    public function checkLogin($username, $password){
        $query = "SELECT idautore, username, nome FROM autore WHERE attivo=1 AND username = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }    
    */

}
?>