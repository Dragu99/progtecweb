-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Sat Dec 26 15:34:09 2020 
-- * LUN file: C:\xampp\htdocs\progtecweb\db\PROJECT.lun 
-- * Schema: ShoppingStore/LOGICO 
-- ********************************************* 


-- Database Section
-- ________________ 

create database ShoppingStore;
use ShoppingStore;


-- Tables Section
-- _____________ 

create table MODELLO (
     codModello varchar(20) not null,
     nome varchar(100) not null,
     prezzo float not null,
     descrizione char(254) not null,
     foto varchar(100) not null,
     marchio char(30) not null,
     constraint IDMODELLO primary key (codModello));

create table PERSONA (
     nome char(20) not null,
     cognome char(50) not null,
     e_mail varchar(50) not null,
     password varchar(80) not null,
     venditore bit(1) not null,
     marchio char(30),
     img varchar(100),
     constraint IDPERSONA primary key (e_mail),
     constraint IDVENDITORE unique (marchio));

create table ORDINE (
     codOrdine int(10) not null,
     codModello varchar(20) not null,
     misura int(2) not null,
     quantità int(3) not null,
     constraint IDORDINE primary key (codOrdine, codModello, misura));

create table ACQUISTO (
     codOrdine int(10) not null auto_increment,
     data datetime not null DEFAULT now(),
     sconto int(3),
     prezzo_tot float not null,
     e_mail varchar(50) not null,
     constraint IDACQUISTO primary key (codOrdine));

create table VERSIONE_MODELLO (
     codModello varchar(20) not null,
     misura int(2) not null,
     quantità int(3) not null,
     constraint IDVERSIONE_MODELLO primary key (codModello, misura));

create table NOTIFICA (
     codNotifica int(10) not null auto_increment,
     destinatario varchar(50) not null,
     data datetime not null DEFAULT now(),
     oggetto varchar(50) not null,
     testo varchar(500) not null,
     codOrdine int(10),
     codSconto varchar(20),
     tipo ENUM('ordine effettuato', 'acquisto prodotto', 'esaurimento scorte', 'benvenuto', 'codice sconto') not null,
     constraint IDNOTIFICA primary key (codNotifica));

create table SCONTO (
     codSconto varchar(20) not null,
     percentuale int(1) not null,
     constraint IDSCONTO primary key (codSconto));
     

-- Constraints Section
-- ___________________ 

alter table MODELLO add constraint FKR
     foreign key (marchio)
     references PERSONA (marchio);

alter table ORDINE add constraint FKord_VER
     foreign key (codModello, misura)
     references VERSIONE_MODELLO (codModello, misura);

alter table ORDINE add constraint FKord_VEN
     foreign key (codOrdine)
     references ACQUISTO (codOrdine);

alter table ACQUISTO add constraint FKacquisto
     foreign key (e_mail)
     references PERSONA (e_mail);

alter table VERSIONE_MODELLO add constraint FKcatalogo
     foreign key (codModello)
     references MODELLO (codModello);

alter table NOTIFICA add constraint FKemail
     foreign key (destinatario)
     references PERSONA (e_mail);

alter table NOTIFICA add constraint FKordine
     foreign key (codOrdine)
     references ACQUISTO (codOrdine);
     
alter table NOTIFICA add constraint FKsconto
     foreign key (codSconto)
     references SCONTO (codSconto);

INSERT INTO `persona` (`cognome`, `e_mail`, `nome`, `password`, `marchio`, `img`, `venditore`) VALUES
('Armani', 'armaniAE@gmail.com', 'Giorgio', '$2y$10$4JK2.8vDsSvWQ5RKXiBOb.cgDnSQiSagE/nFMUDeHNjzAC3AFslV.', 'Giorgio Armani', 'marchioArmani.jpg', 1),
('Chanel', 'coco@gmail.com', 'Coco', '$2y$10$9e.erNk4q/hQQicGco3aH.0TI1HHp8lOfpOlkk/Iani8.WM0QGNoa', 'Chanel', 'marchioChanel.jpg', 1),
('Kors', 'michKors@gmail.com', 'Michael', '$2y$10$p6utohqdlDQXNCS.jTir6.CSQ0B9HHarLqX5GTzF8NOCD01TVzlH6', 'Michael Kors', 'marchioKors.jpg', 1),
('Louboutin', 'chrissShoes@gmail.com', 'Christian', '$2y$10$l5BLSjwiUgu/ZyuQfzI5leKdJpM/xZq2fYXGAcQOUUebRkVfODPDG', 'Louboutin', 'marchioLouboutin.jpg', 1),
('Yves Saint', 'YSL@gmail.com', 'Laurent', '$2y$10$WqhyUqvmuIpgT8O2WqZHa./rzDahotoKuVbwLXb9gj62gPV5glMhC', 'Yves Saint Laurent', 'marchioYSL.jpg', 1);

INSERT INTO `modello` (`codModello`, `descrizione`, `marchio`, `prezzo`, `foto`, `nome`) VALUES
('1a', 'Stivaletti in pelle nera, tacco 7 rettangolare, con logo nel cinturino', 'Giorgio Armani', '110.00', 'armani1.jpg', 'Armani - 1a'),
('2a', 'Mary Poppins bicolore, nude e nero, tacco 6 rettangolare, con lacci frontali', 'Giorgio Armani', '180.00', 'armani2.jpg', 'Armani - 2a'),
('3a', 'Decollettè in raso azzurro, tacco 7 spillo, con logo nel cinturino', 'Giorgio Armani', '320.00', 'armani3.jpg', 'Armani - 3a'),
('4a', 'Sandali open TOE bianchi, tacco 10 spillo, con cinturino alla caviglia nero', 'Giorgio Armani', '490.00', 'armani4.jpg', 'Armani - 4a'),
('5a', 'Chanel in principe di galles, tacco 5 spillo, con nastro in punta nero', 'Giorgio Armani', '520.00', 'armani5.jpg', 'Armani - 5a'),
('6a', 'Sandali in camoscio beige, tacco 7, con logo nel cinturino', 'Giorgio Armani', '210.00', 'armani6.jpg', 'Armani - 6a'),
('7a', 'Decollettè con plateaux 1cm, ghepardate, tacco 7', 'Giorgio Armani', '300.00', 'armani7.jpg', 'Armani - 7a'),
('8a', 'Zeppe in sughero di 5 cm, con logo posteriore e nastro nero', 'Giorgio Armani', '100.00', 'armani8.jpg', 'Armani - 8a'),

('1c', 'Chanel bicolore, nude e punta nera, tacco 3 rettangolare, con cinturino monolato', 'Chanel', '340.00', 'chanel1.jpg', 'Chanel - 1c'),
('2c', 'Decollettè bicolore, bianco e punta nera, tacco 5 annodato', 'Chanel', '500.00', 'chanel2.jpg', 'Chanel - 2c'),
('3c', 'Decollettè bicolore, nude e punta nera, tacco 5 con plateaux scozzese', 'Chanel', '110.00', 'chanel3.jpg', 'Chanel - 3c'),
('4c', 'Chanel bicolore, binache e punta nera, tacco 3 con perla, con cinturino monolato', 'Chanel', '490.00', 'chanel4.jpg', 'Chanel - 4c'),
('5c', 'Chanel in pelle di pitone, tacco 3 spillo, con cinturino nero', 'Chanel', '530.00', 'chanel5.jpg', 'Chanel - 5c'),
('6c', 'Stivaletti in camoscio nero, tacco 5 rettangolare, con logo nel cinturino', 'Chanel', '210.00', 'chanel6.jpg', 'Chanel - 6c'),
('7c', 'Chanel nere, tacco 3 spillo, con cinturino e fiocco frontale', 'Chanel', '350.00', 'chanel7.jpg', 'Chanel - 7c'),
('8c', 'Zeppe di 5 cm, con logo posteriore in camoscio asfaltato', 'Chanel', '190.00', 'chanel8.jpg', 'Chanel - 8c'),

('1m', 'Stivaletti in pelle nera, tacco 5 rettangolare, con cinturino in pelle con logo', 'Michael Kors', '280.00', 'kors1.jpg', 'Michael Kors - 1m'),
('2m', 'Sandali in pelle nera, tacco 7 dorato', 'Michael Kors', '390.00', 'kors2.jpg', 'Michael Kors - 2m'),
('3m', 'Sandali in pelle nude, tacco 7 argentato', 'Michael Kors', '390.00', 'kors3.jpg', 'Michael Kors - 3m'),
('4m', 'Decollettè bicolore, nude e punta nera, tacco 5 spillo', 'Michael Kors', '420.00', 'kors4.jpg', 'Michael Kors - 4m'),
('5m', 'Decollettè bicolore, nere e punta argentata, tacco 5 spillo', 'Michael Kors', '420.00', 'kors5.jpg', 'Michael Kors - 5m'),
('6m', 'Sandali brillantinati in fondo nude, tacco 7 rettangolare, con doppio cinturino', 'Michael Kors', '370.00', 'kors6.jpg', 'Michael Kors - 6m'),
('7m', 'Stivaletti in camoscio beuge, tacco 5 trapezioidale', 'Michael Kors', '280.00', 'kors7.jpg', 'Michael Kors - 7m'),
('8m', 'Stivaletti in pelle nude, tacco 5 trapezioidale dorato', 'Michael Kors', '300.00', 'kors8.jpg', 'Michael Kors - 8m'),

('1l', 'Decollettè in pelle nera lucida, tacco 12 spillo', 'Louboutin', '610.00', 'louboutin1.jpg', 'Louboutin - 1l'),
('2l', 'Decollettè in pelle bianca lucida, tacco 10 spillo', 'Louboutin', '550.00', 'louboutin2.jpg', 'Louboutin - 2l'),
('3l', 'Decollettè in pelle, rosso sfumato nero, tacco 10 spillo', 'Louboutin', '730.00', 'louboutin3.jpg', 'Louboutin - 3l'),
('4l', 'Decollettè bicolore, bianche e tacco ghepardato, tacco 10 spillo', 'Louboutin', '800.00', 'louboutin4.jpg', 'Louboutin - 4l'),
('5l', 'Sandali trasparenti, con decorazioni nere in punta, tacco 10 spillo, nastro in raso alla caviglia', 'Louboutin', '680.00', 'louboutin5.jpg', 'Louboutin - 5l'),
('6l', 'Decollettè in pelle rossa lucida, con mini plateaux bianco, tacco 10 spillo', 'Louboutin', '590.00', 'louboutin6.jpg', 'Louboutin - 6l'),
('7l', 'Decollettè in pelle azzurra lucida, con mini plateaux bianco, tacco 10 spillo', 'Louboutin', '590.00', 'louboutin7.jpg', 'Louboutin - 7l'),
('8l', 'Sandali in pelle nude, tacco 10 spillo, cinturino su collo del piede', 'Louboutin', '380.00', 'louboutin8.jpg', 'Louboutin - 8l'),

('1y', 'Sandali in pelle nera, tacco dorato con logo YSL', 'Yves Saint Laurent', '1010.00', 'ysl1.jpg', 'YSL - 1y'),
('2y', 'Sandali in pelle nera, tacco nero con logo YSL', 'Yves Saint Laurent', '1010.00', 'ysl2.jpg', 'YSL - 2y'),
('3y', 'Decollettè in pelle nera, tacco nero con logo YSL', 'Yves Saint Laurent', '950.00', 'ysl3.jpg', 'YSL - 3y'),
('4y', 'Decollettè in pelle bianca, tacco nero con logo YSL', 'Yves Saint Laurent', '950.00', 'ysl4.jpg', 'YSL - 4y'),
('5y', 'Sandali in pelle nera, tacco 10 spillo, cinturino con logo', 'Yves Saint Laurent', '780.00', 'ysl5.jpg', 'YSL - 5y'),
('6y', 'Chanel spezzate in pelle nera lucida, tacco dorato con logo YSL', 'Yves Saint Laurent', '590.00', 'ysl6.jpg', 'YSL - 6y'),
('7y', 'Stivaletti in pelle nera, tacco dorato con logo YSL', 'Yves Saint Laurent', '470.00', 'ysl7.jpg', 'YSL - 7y'),
('8y', 'Stivaletti in pelle nera lucida, elastico laterale, tacco bianco con logo YSL', 'Yves Saint Laurent', '490.00', 'ysl8.jpg', 'YSL - 8y');

INSERT INTO `versione_modello` (`codModello`, `quantità`, `misura`) VALUES
('1a', 10, 36),
('1a', 10, 37),
('1a', 10, 38),
('1a', 10, 39),
('1a', 10, 40),
('1a', 10, 41),
('2a', 10, 36),
('2a', 10, 37),
('2a', 10, 38),
('2a', 10, 39),
('2a', 10, 40),
('2a', 10, 41),
('3a', 10, 36),
('3a', 10, 37),
('3a', 10, 38),
('3a', 10, 39),
('3a', 10, 40),
('3a', 10, 41),
('4a', 10, 36),
('4a', 10, 37),
('4a', 10, 38),
('4a', 10, 39),
('4a', 10, 40),
('4a', 10, 41),
('5a', 10, 36),
('5a', 10, 37),
('5a', 10, 38),
('5a', 10, 39),
('5a', 10, 40),
('5a', 10, 41),
('6a', 10, 36),
('6a', 10, 37),
('6a', 10, 38),
('6a', 10, 39),
('6a', 10, 40),
('6a', 10, 41),
('7a', 10, 36),
('7a', 10, 37),
('7a', 10, 38),
('7a', 10, 39),
('7a', 10, 40),
('7a', 10, 41),
('8a', 10, 36),
('8a', 10, 37),
('8a', 10, 38),
('8a', 10, 39),
('8a', 10, 40),
('8a', 10, 41),

('1c', 10, 36),
('1c', 10, 37),
('1c', 10, 38),
('1c', 10, 39),
('1c', 10, 40),
('1c', 10, 41),
('2c', 10, 36),
('2c', 10, 37),
('2c', 10, 38),
('2c', 10, 39),
('2c', 10, 40),
('2c', 10, 41),
('3c', 10, 36),
('3c', 10, 37),
('3c', 10, 38),
('3c', 10, 39),
('3c', 10, 40),
('3c', 10, 41),
('4c', 10, 36),
('4c', 10, 37),
('4c', 10, 38),
('4c', 10, 39),
('4c', 10, 40),
('4c', 10, 41),
('5c', 10, 36),
('5c', 10, 37),
('5c', 10, 38),
('5c', 10, 39),
('5c', 10, 40),
('5c', 10, 41),
('6c', 10, 36),
('6c', 10, 37),
('6c', 10, 38),
('6c', 10, 39),
('6c', 10, 40),
('6c', 10, 41),
('7c', 10, 36),
('7c', 10, 37),
('7c', 10, 38),
('7c', 10, 39),
('7c', 10, 40),
('7c', 10, 41),
('8c', 10, 36),
('8c', 10, 37),
('8c', 10, 38),
('8c', 10, 39),
('8c', 10, 40),
('8c', 10, 41),

('1m', 10, 36),
('1m', 10, 37),
('1m', 10, 38),
('1m', 10, 39),
('1m', 10, 40),
('1m', 10, 41),
('2m', 10, 36),
('2m', 10, 37),
('2m', 10, 38),
('2m', 10, 39),
('2m', 10, 40),
('2m', 10, 41),
('3m', 10, 36),
('3m', 10, 37),
('3m', 10, 38),
('3m', 10, 39),
('3m', 10, 40),
('3m', 10, 41),
('4m', 10, 36),
('4m', 10, 37),
('4m', 10, 38),
('4m', 10, 39),
('4m', 10, 40),
('4m', 10, 41),
('5m', 10, 36),
('5m', 10, 37),
('5m', 10, 38),
('5m', 10, 39),
('5m', 10, 40),
('5m', 10, 41),
('6m', 10, 36),
('6m', 10, 37),
('6m', 10, 38),
('6m', 10, 39),
('6m', 10, 40),
('6m', 10, 41),
('7m', 10, 36),
('7m', 10, 37),
('7m', 10, 38),
('7m', 10, 39),
('7m', 10, 40),
('7m', 10, 41),
('8m', 10, 36),
('8m', 10, 37),
('8m', 10, 38),
('8m', 10, 39),
('8m', 10, 40),
('8m', 10, 41),

('1l', 10, 36),
('1l', 10, 37),
('1l', 10, 38),
('1l', 10, 39),
('1l', 10, 40),
('1l', 10, 41),
('2l', 10, 36),
('2l', 10, 37),
('2l', 10, 38),
('2l', 10, 39),
('2l', 10, 40),
('2l', 10, 41),
('3l', 10, 36),
('3l', 10, 37),
('3l', 10, 38),
('3l', 10, 39),
('3l', 10, 40),
('3l', 10, 41),
('4l', 10, 36),
('4l', 10, 37),
('4l', 10, 38),
('4l', 10, 39),
('4l', 10, 40),
('4l', 10, 41),
('5l', 10, 36),
('5l', 10, 37),
('5l', 10, 38),
('5l', 10, 39),
('5l', 10, 40),
('5l', 10, 41),
('6l', 10, 36),
('6l', 10, 37),
('6l', 10, 38),
('6l', 10, 39),
('6l', 10, 40),
('6l', 10, 41),
('7l', 10, 36),
('7l', 10, 37),
('7l', 10, 38),
('7l', 10, 39),
('7l', 10, 40),
('7l', 10, 41),
('8l', 10, 36),
('8l', 10, 37),
('8l', 10, 38),
('8l', 10, 39),
('8l', 10, 40),
('8l', 10, 41),

('1y', 10, 36),
('1y', 10, 37),
('1y', 10, 38),
('1y', 10, 39),
('1y', 10, 40),
('1y', 10, 41),
('2y', 10, 36),
('2y', 10, 37),
('2y', 10, 38),
('2y', 10, 39),
('2y', 10, 40),
('2y', 10, 41),
('3y', 10, 36),
('3y', 10, 37),
('3y', 10, 38),
('3y', 10, 39),
('3y', 10, 40),
('3y', 10, 41),
('4y', 10, 36),
('4y', 10, 37),
('4y', 10, 38),
('4y', 10, 39),
('4y', 10, 40),
('4y', 10, 41),
('5y', 10, 36),
('5y', 10, 37),
('5y', 10, 38),
('5y', 10, 39),
('5y', 10, 40),
('5y', 10, 41),
('6y', 10, 36),
('6y', 10, 37),
('6y', 10, 38),
('6y', 10, 39),
('6y', 10, 40),
('6y', 10, 41),
('7y', 10, 36),
('7y', 10, 37),
('7y', 10, 38),
('7y', 10, 39),
('7y', 10, 40),
('7y', 10, 41),
('8y', 10, 36),
('8y', 10, 37),
('8y', 10, 38),
('8y', 10, 39),
('8y', 10, 40),
('8y', 10, 41);

INSERT INTO `notifica` (`destinatario`, `oggetto`, `testo`) VALUES
('armaniAE@gmail.com', 'Benvenuto in La Rinascente Shop & Shoes online', 'Benvenuto sig.Armani, siamo lieti di darle il benvenuto a La Rinascente Shoes & Shop Online'),
('coco@gmail.com', 'Benvenuto in La Rinascente Shop & Shoes online', 'Benvenuta sig.Chanel, siamo lieti di darle il benvenuto a La Rinascente Shoes & Shop Online'),
('michKors@gmail.com', 'Benvenuto in La Rinascente Shop & Shoes online', 'Benvenuto sig.Kors, siamo lieti di darle il benvenuto a La Rinascente Shoes & Shop Online'),
('chrissShoes@gmail.com', 'Benvenuto in La Rinascente Shop & Shoes online', 'Benvenuto sig.Louboutin, siamo lieti di darle il benvenuto a La Rinascente Shoes & Shop Online'),
('YSL@gmail.com', 'Benvenuto in La Rinascente Shop & Shoes online', 'Benvenuto sig.Laurent, siamo lieti di darle il benvenuto a La Rinascente Shoes & Shop Online');

INSERT INTO `sconto`(`codSconto`, `percentuale`) VALUES 
('benvenuto10','10.00'), 
('regalo20','20.00');
