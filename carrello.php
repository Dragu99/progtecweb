<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("location: index.php");
}

if(isset($_SESSION["giàscontato"])) {
    unset($_SESSION["giàscontato"]);
}

//Base Template
$templateParams["pagina"]="carrello";
$templateParams["js"] = array("js/jquery-3.5.1.min.js","js/carrello.js");

require 'template/baseSmallHeaderBottoni.php';
?>