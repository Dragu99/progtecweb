<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("location: index.php");
}
//Base Template
$templateParams["js"] = array("js/jquery-3.5.1.min.js","js/dynamicVenditore.js");
$templateParams["smallHeader"] = "mostra";
$templateParams["venditore"] = "mostra";

if(isset($_GET["store"])){
    $templateParams["titolo"] = $_GET["store"]." Seller";
    $templateParams["header"] = $_GET["store"];
}

if(isset($_SESSION["nuovoProdottoAggiunto"])){
    $templateParams["msg"] = $_SESSION["nuovoProdottoAggiunto"];
    unset($_SESSION["nuovoProdottoAggiunto"]);
}

require 'template/baseBottoni.php';
?>