<?php
require_once '../bootstrap.php';

if(isset($_GET["store"])){
    $store = $_GET["store"];
    $prodotti = $dbh->getShoppingProducts($store);
} else {
    $prodotti = $dbh->getAllShoppingProducts();
}

for($i = 0; $i < count($prodotti); $i++){
    $prodotti[$i]["foto"] = UPLOAD_DIR.$prodotti[$i]["foto"];
}
header('Content-Type: application/json');
echo json_encode($prodotti);
?>