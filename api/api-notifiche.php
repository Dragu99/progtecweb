<?php
	require_once '../bootstrap.php';
	$notifiche = $dbh->getUserNotification($_SESSION["user"]);
	header('Content-Type: application/json');

	echo json_encode($notifiche);
?>