<?php
require_once '../bootstrap.php';

$store="nessuno";
if(isset($_GET["store"])){
    $store = $_GET["store"];
}
$otherStores = $dbh->getStores($store);

for($i = 0; $i < count($otherStores); $i++){
    $otherStores[$i]["img"] = UPLOAD_DIR.$otherStores[$i]["img"];
}

header('Content-Type: application/json');
echo json_encode($otherStores);
?>