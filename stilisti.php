<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("location: index.php");
}
//Base Template
$templateParams["titolo"] = "Stilisti";
$templateParams["header"] = "La Rinascente Shoes & Shop online";
$templateParams["js"] = array("js/jquery-3.5.1.min.js","js/dynamicStilisti.js");


require 'template/baseStilisti.php';
?>