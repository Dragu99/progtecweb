<?php
function sec_session_start() {
    $session_name = 'sec_session_id'; // Imposta un nome di sessione
    $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
    $httponly = true; // Questo impedirà ad un javascript di essere in grado di accedere all'id di sessione.
    ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
    $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
    session_name($session_name); // Imposta il nome di sessione con quello prescelto all'inizio della funzione.
    session_start(); // Avvia la sessione php.
    session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}

function registerLoggedUser($email){
    $_SESSION["user"] = $email;
}

function isUserLoggedIn(){
    return !empty($_SESSION["user"]);
}

function dateDiffFromNow($data) {
    $date = new DateTime($data);
    $now = new DateTime();
    $diff = date_diff($date, $now);
    return $diff->format("%a");
}

function getNotifica($tipo, $codOrdine=NULL, $prodotto=NULL) {
    $notifica;
    switch($tipo) {
        case "benvenuto":
            $notifica["oggetto"] = 'Benvenuto in La Rinascente Shoes & Shop Online';
            $notifica["testo"] = 'Siamo lieti di darle il benvenuto in La Rinascente Shoes & Shop Online, da questo momento può acquistare i nostri prodotti!<br/>Inoltre ecco un buono sconto del 10% :)<br/>Codice sconto: BENVENUTO10';
            break;
        case "ordine effettuato":
            $notifica["oggetto"] = 'Grazie per aver acquistato da noi!';
            $notifica["testo"] = 'Hey, abbiamo preso in carico il tuo ordine '.$codOrdine.'. Arriverà presto!';
            break;
        case "acquisto prodotto":
            $notifica["oggetto"] = 'Ciaooo, abbiamo buone notizie!';
            $notifica["testo"] = 'Qualcuno ha acquistato un tuo prodotto!<br/>Nello specifico: '.$prodotto["codModello"].' nella taglia '.$prodotto["misura"];
            break;
        case "esaurimento scorte":
            $notifica["oggetto"] = "Presto! C'è bisogno di te";
            $notifica["testo"] = 'Uno dei tuoi prodotti è esaurito.<br/>Stiamo parlando del prodotto: '.$prodotto["codModello"].' nella taglia '.$prodotto["misura"];
            break;
        case "codice sconto":
            $notifica["oggetto"] = 'Ciao, ci sei mancato!';
            $notifica["testo"] = 'Oggi è un giorno felice :)<br/>Ecco a te uno sconto del 20%<br/>Codice sconto: REGALO20';
            break;
    }
    return $notifica;
}

function uploadImage($path, $image){
    $imageName = basename($image["name"]);
    $fullPath = $path.$imageName;
    
    $maxKB = 500;
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    $result = 0;
    $msg = "";
    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if($imageSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }
    //Controllo dimensione dell'immagine < 500KB
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($imageFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
        }
        while(file_exists($path.$imageName));
        $fullPath = $path.$imageName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($image["tmp_name"], $fullPath)){
            $msg.= "Errore nel caricamento dell'immagine.";
        }
        else{
            $result = 1;
            $msg = $imageName;
        }
    }
    return array($result, $msg);
}

?>
