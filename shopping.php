<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("location: index.php");
}
//Base Template
$templateParams["js"] = array("js/jquery-3.5.1.min.js","js/dynamicShopping.js");
$templateParams["smallHeader"] = "mostra";

if(isset($_GET["store"])){
    $templateParams["titolo"] = $_GET["store"]." Store";
    $templateParams["header"] = $_GET["store"];
}

require 'template/baseBottoni.php';
?>